import csv
from Link import CoreOp
import json
import os


class Prettyfy:
    """This class is used to take a dictionnary from the Link class
    and transform it into json.
    We first make a list of the dictionnary then transform the list into
    a nasted dictionnary before finally jsonifying it"""

    def __init__(self):
        self.lien = CoreOp()

    def fetch_ophish_url(self):
        with open('dataset.csv') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            filename = "dataset.json"
            cpt = 0

            with open(filename, 'w') as f:
                f.write('{')
                f.close()

            for row in spamreader:
                try:
                    response = self.lien.merge_dicts(row[0])
                    response["isPhishing"] = row[1]
                    with open(filename, 'a') as f:
                        towrite = '\"' + str(cpt) + '\": '
                        f.write(towrite)
                        json.dump(response, f)
                        f.write(',')
                    print(response)
                except Exception as e:
                    print(e)
                cpt += 1

            try:
                with open(filename, 'rb+') as filehandle:
                    filehandle.seek(-1, os.SEEK_END)
                    filehandle.truncate()
            except Exception:
                pass

            with open(filename, 'a') as file:
                file.write("}")
                file.close()


if __name__ == '__main__':
    self = Prettyfy()
    self.fetch_ophish_url()
