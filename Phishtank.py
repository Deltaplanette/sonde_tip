from time import sleep
from urllib import request

import requests
from bs4 import BeautifulSoup


class Phish:
    """This class is the main class of the first probe.
    This is where the main investigations functions are done."""

    def __init__(self, url):
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Macin53tosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko)'
                          'Chrome/70.0.3538.77 Safari/7.36'
        }
        self.url = url

    def check_if_page_up(url):
        """This function verify if the page is up or down.
        We use the status code 200 to see if the request is
        successfull or not"""
        page_request = requests.get(url=url)
        if page_request.status_code == 200:
            return page_request.text
        else:
            return None

    def open_url(self):
        """This function is used to open the page and get the
        html code of the page"""
        page_response = Phish.check_if_page_up(self.url)
        if page_response:
            page_request = request.Request(self.url, headers=self.headers)
            sleep(1)
            page = request.urlopen(page_request)
            self.page = page
            return page

    def grab_phish_url(self):
        """This function is used to get the phishtank links where we
        can find the submitter and the potential phishing link.
        We grab the "number" of the page and then add it to the phishtank url."""
        phish_ids = []
        page = BeautifulSoup(self.open_url(), 'html.parser')
        all_links = page.find_all('a')
        for link in all_links:
            link = link.get('href')
            if 'phish_detail.php?phish_id=' in link:
                phish_ids.append("https://www.phishtank.com/" + link)
        return phish_ids

    def grab_url_and_submitter(self):
        """This function is used to get the phishing url from the links given by the previous
        function.
        We use here bs4 and scrap for the "b" tags"""
        phish_url = []
        phish_ids = self.grab_phish_url()
        for phish_urls in phish_ids:
            url = Phish(phish_urls)
            sleep(1)
            open = url.open_url()
            page = BeautifulSoup(open, 'html.parser')
            phish_url.append(page.b.findNext("b").text)
        return phish_url
